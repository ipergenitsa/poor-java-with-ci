package com.gitlab.ipergenitsa;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import static org.junit.Assert.assertThat;

public class MainTest {
    @Test
    public void testSum() {
        double sum = Main.sum(1, 2);
        assertThat("1 + 2 should be 3", sum, CoreMatchers.equalTo(3.0));
        System.out.println("Test is finished");
    }
}
